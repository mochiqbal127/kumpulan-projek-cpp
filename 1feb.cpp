#include <conio.h>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

typedef struct{
	int nisn;
	string nama;
	char jk;
	double bing;
	double bindo;
	double mtk;
	double psikotest;
	double total_nilai;
	char sikap;
	string status;
} siswa;

siswa data_psb[100];
int jml_data;

void menu(){
	system("CLS");
	cout<<"============================\n";
	cout<<"MENU\n";
	cout<<"============================\n";
	cout<<"1. Entry data awal\n";
	cout<<"2. Entry nilai psikotest dan sikap\n";
	cout<<"3. Tampil data\n";
	cout<<"4. Tampil data kelulusan (sorting)\n";
	cout<<"5. Cek kelulusan perorang\n";
	cout<<"6. Simpan data ke file\n";
	cout<<"7. Baca data dari file\n";
	cout<<"8. Keluar\n";
	cout<<endl;
}
void input_data(){
	system("CLS");
	char ulangi='y';
	int a = 0;
	do{
		cout<<endl;
		cout<<"NISN	: "; cin>>data_psb[a].nisn;
		cout<<"Nama	: "; cin>>data_psb[a].nama;
		cout<<"JK	: "; cin>>data_psb[a].jk;
		cout<<"Nilai B. Indonesia	: "; cin>>data_psb[a].bindo;
		cout<<"Nilai B. Inggris	: "; cin>>data_psb[a].bing;
		cout<<"Nilai Matematika	: "; cin>>data_psb[a].mtk;
		a++;
		cout<<"\nTambah data (y/n)	: "; cin>>ulangi;
	}while(ulangi=='y'||ulangi=='Y');
	jml_data += a;
}
int search(siswa a[], int jml, int target){
	for(int n = jml - 1 ; n> -2	; n--)
		if(n == -1 || a[n].nisn == target) return n;
}
string cekLulus(int index){
	double rataUN = (data_psb[index].bindo+data_psb[index].bing+data_psb[index].mtk)/3;
	if(rataUN>=55){
		if(data_psb[index].psikotest >= 60 && (data_psb[index].sikap=='A'||data_psb[index].sikap=='B'||data_psb[index].sikap=='C')){
			return "LULUS";
		}
	}
	return "TIDAK LULUS";
}
int sorting(){
	for (int i = 1;i<jml_data;i++){
		for (int j=jml_data-1;j>=i;j--){
			if(data_psb[j].total_nilai > data_psb[j-1].total_nilai){
				data_psb[70] = data_psb[j];
				data_psb[j] = data_psb[j-1];
				data_psb[j-1]= data_psb[70];
			}
		}
	}
}
void input_nilai(){
	system("CLS");
	char ulangi = 'y';
	int nisn;
	int index = 0;
	do{
		cout<<"NISN	: "; cin>>nisn;
		index = search(data_psb,jml_data,nisn);
		cout<<"Nama	: "<<data_psb[index].nama<<endl;
		cout<<"Nilai Psikotest	: "; cin>>data_psb[index].psikotest;
		cout<<"Nilai Sikap	: "; cin>>data_psb[index].sikap;
		data_psb[index].status = cekLulus(index);
		data_psb[index].total_nilai = data_psb[index].bindo+data_psb[index].bing+data_psb[index].mtk+data_psb[index].psikotest;
		cout<<"\nTambah Data (y/n)	: "; cin>>ulangi;
	}while(ulangi=='y'||ulangi=='Y');
}
void tampil_data(){
	system("CLS");
	cout<<"Data Calon Siswa Baru\n";
	cout<<"------------------------------------\n\n";
	cout<<"No. NISN\t Nama\t JK\t N.Indo\t N.Ing\t N.MTK\t N.Psiko\t N.Sikap\t Jumlah\n";
	cout<<"-------------------------------------------------------------------------------------------\n";
	for(int a = 0 ; a<jml_data ; a++){
		double jumlah = data_psb[a].bindo + data_psb[a].bing + data_psb[a].mtk + data_psb[a].psikotest;
		cout<<a+1<<" ";
		cout<<data_psb[a].nisn<<"\t";
		cout<<data_psb[a].nama<<"\t";
		cout<<data_psb[a].jk<<"\t";
		cout<<data_psb[a].bindo<<"\t";
		cout<<data_psb[a].bing<<"\t";
		cout<<data_psb[a].mtk<<"\t";
		cout<<data_psb[a].psikotest<<"\t";
		cout<<data_psb[a].sikap<<"\t";
		cout<<jumlah<<"\t\n";
	}
}
void tampil_data_lulus(){
	sorting();
	system("CLS");
	cout<<"Data Kelulusan Siswa Baru (Sorted)\n";
	cout<<"--------------------------------\n\n";
	cout<<"No.	NISN\t Nama\t JK\t Jumlah Nilai\t Status\n";
	cout<<"------------------------------------------------\n";
	for (int a = 0; a < jml_data; a++){
		double jumlah = data_psb[a].bindo + data_psb[a].bing + data_psb[a].mtk + data_psb[a].psikotest;
		cout<<a+1<<" ";
		cout<<data_psb[a].nisn<<"\t";
		cout<<data_psb[a].nama<<"\t";
		cout<<data_psb[a].jk<<"\t";
		cout<<data_psb[a].total_nilai<<"\t";
		cout<<data_psb[a].status<<"\t\n";

	}
	
}
void cekLulusPerorang(){
	system("CLS");
	char ulangi = 'y';
	int nisn;
	int index = 0;
	do{
		cout<<"NISN	: "; cin>>nisn;
		index = search(data_psb,jml_data,nisn);
		cout<<"Nama	: "<<data_psb[index].nama<<endl;
		cout<<"Nilai B. Indo	: "<<data_psb[index].bindo<<endl;
		cout<<"Nilai B. Inggris	: "<<data_psb[index].bing<<endl;
		cout<<"Nilai Matematika	: "<<data_psb[index].mtk<<endl;
		cout<<"Nilai Psikotest	: "<<data_psb[index].psikotest<<endl;
		cout<<"Nilai Sikap	: "<<data_psb[index].sikap<<endl;
		cout<<"Nilai Total Nilai	: "<<data_psb[index].total_nilai<<endl;
		cout<<"Nilai Status	: "<<data_psb[index].status<<endl;
		cout<<"\nCari Data (y/n)	: "; cin>>ulangi;
	}while(ulangi=='y'||ulangi=='Y');
}

void readFile(){
	system("CLS");
	cout<<"Membaca isi savedfilepbs.txt\n\n";
	string myFile;
	ifstream MyReadFile("savedfilepbs.txt");
	while (getline (MyReadFile, myFile)) {
  		cout<<myFile<<endl;
	}
	MyReadFile.close();

}
void saveToFile(){
//	sorting();
	system("CLS");
	cout<<"Menyimpan data ke savedfilepbs.txt\n";
	ofstream MyFile("savedfilepbs.txt");
	
	if(jml_data != 0 && MyFile.is_open()){
		for (int a = 0; a < jml_data; a++){
			double jumlah = data_psb[a].bindo + data_psb[a].bing + data_psb[a].mtk + data_psb[a].psikotest;
	//		cout<<a+1<<" ";
	//		cout<<data_psb[a].nisn<<"\t";
	//		cout<<data_psb[a].nama<<"\t";
	//		cout<<data_psb[a].jk<<"\t";
	//		cout<<data_psb[a].total_nilai<<"\t";
	//		cout<<data_psb[a].status<<"\t\n";
			
			MyFile<<a+1<<"	"
			<<"NISN	: "<<data_psb[a].nisn<<"\n"
			<<"Nama	: "<<data_psb[a].nama<<"\n"
			<<"JK	: "<<data_psb[a].jk<<"\n"
			<<"Total Nilai	: "<<data_psb[a].total_nilai<<"\n"
			<<"Status	: "<<data_psb[a].status<<"\n"
			;
		}
		cout<<"\nData Exported\n";
	}
	else{
		cout<<"Unable to open file";
	}
	
  	system("pause");
}

int main(){
	int pilihMenu;
	char backToMenu='y';
	system("COLOR F0");
	system("CLS");
	
	do{
		menu();
		cout<<endl<<"Pilih menu (1-7) : "; cin>>pilihMenu;
		
		switch(pilihMenu) {
			system("COLOR F0");
		case 1 :input_data();
			break;			
		case 2 :input_nilai();
			break;			
		case 3 :tampil_data();
			break;			
		case 4 :tampil_data_lulus();
			break;			
		case 5 :cekLulusPerorang();
			break;		
		case 6 :saveToFile(); 
			break;			
		case 7 :readFile();
			break;
		case 8 :return 0;
			break;
		default : cout<<"\nPilihan Menu Tidak Ada!\n"<<endl;
	}
		cout<<"Kembali ke menu (y/n) : "; cin>>backToMenu;
	}while(backToMenu == 'y' || backToMenu == 'Y');
	
	
}
