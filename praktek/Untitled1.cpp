#include <iostream>
#include <conio.h>
#include <iomanip>

using namespace std;

int main(){
	//deklarasi //inisiasi
	int nilai[10] = {86,87,70,96,77};
	//pengisian nilai
	nilai[5] = 90;
	//menampilkan nilai
	cout<<nilai[0]<<endl;
	cout<<nilai[2]<<endl;
	cout<<nilai[4]<<endl;
	//mengisi dengan cara diinput user
	cout<<"masukan nilai ke-8 : "; cin>>nilai[7];
	cout<<"masukan nilai ke-9 : "; cin>>nilai[8];
	cout<<"masukan nilai ke-10 : "; cin>>nilai[9];
	//mengisi dengan cara diinput user melalui pengulangan
	for(int i=0; i<10; i++){
		cout<<"masukan nilai ke-"<<i+1<<" : ";
		cin>>nilai[i];
	}
}
