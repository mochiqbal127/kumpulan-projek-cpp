#include <iostream>
#include <conio.h>
#include <string>
#include <iomanip>

using namespace std;

//Biodata
void nampilnama(string nama, string kelas){
	cout<<"Nama 	= "<<nama<<endl;
	cout<<"Kelas 	= "<<kelas<<endl;	
}
//1.Menampilkan 2 Angka
void nampilangka(int nilai1, int nilai2){
	cout<<"Masukan Angka ke-1 = "; cin>>nilai1;
	cout<<"Masukan Angka ke-2 = "; cin>>nilai2;
		
	cout<<"Angka ke-1 = "<<nilai1<<endl;
	cout<<"Angka ke-2 = "<<nilai2<<endl;
}
//2.Menentukan Luas Lingkaran
float luasling(double jari){
	float luas,phi;
	
	cout<<"Masukan Jari-Jari: "; cin>>jari;
	
	phi = 3.14;
	
	luas = phi*jari*jari;
	cout<<"Luasnya adalah = ";
	return luas;

}
//3.Menentukan Diskon
float diskon(float jumlahbayar){
	float total;
	
	cout<<"Masukan Jumlah Bayar = "; cin>>jumlahbayar;
	
	cout<<"Diskon ";
	
	if(jumlahbayar>=1000000)
		cout<<"5% dari "<<jumlahbayar<<" = ";
	else if(jumlahbayar>=100000)
		cout<<"2.5% dari "<<jumlahbayar<<" = ";
	
	
	if(jumlahbayar>=1000000)
		return jumlahbayar*5/100;
	else if(jumlahbayar>=100000)
		return jumlahbayar*2.5/100;
	else
		return 0;	
}
//Menentukan Banyak Poin
int banyakpoin(int jumlahbayar){
	int poin;
	
	cout<<"Masukan Jumlah Bayar = "; cin>>jumlahbayar;
	poin = jumlahbayar / 20000;
	cout<<"Poin yang didapat	= ";
	return poin;
	
}

int main(){
	cout<<fixed<<setprecision(0);
	cout<<"================================================ Biodata ====================================================="<<endl;
	nampilnama("Iqbal Maulana Muhammad","X-RPL 1");
	cout<<"========================================== 1.Menampilkan 2 Nilai ============================================="<<endl;	
	nampilangka(1,2);
	cout<<"========================================== 2.Menentukan Luas Lingkaran ======================================="<<endl;	
	cout<<luasling(10)<<endl;
	cout<<"========================================== 3.Menentukan Diskon ==============================================="<<endl;
	cout<<diskon(0)<<endl;
	cout<<"========================================== 4.Menentukan Banyak Poin =========================================="<<endl;
	cout<<banyakpoin(1000);
}
