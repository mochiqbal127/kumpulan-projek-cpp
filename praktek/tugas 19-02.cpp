#include <iostream>
#include <conio.h>
#include <string>
#include <iomanip>

using namespace std;

double menentukan_nilai_akhir(){
	double nilai_harian,uts,uas,jumlah,nilai_akhir;
	
	cout<<"Masukan Nilai Harian 	: "; cin>>nilai_harian;
	cout<<"Masukan Nilai UTS 	: "; cin>>uts;
	cout<<"Masukan Nilai UAS 	: "; cin>>uas;
	
	
	cout<<"Nilai Akhir 		: ";
	return (nilai_harian+uts+uas)/3;
}
string menentukan_tahun_kabisat(){
	int tahun;
	
	cout<<"Masukan Tahun : "; cin>>tahun;
	
	cout<<tahun<<" Adalah ";
	
	if(tahun % 4 == 0 )
		return "Tahun Kabisat";
	else
		return "Bukan Tahun Kabisat";
}
int main(){
	system ("Color 08");
	cout<<fixed<<setprecision(1);
	cout<<"=========================================1.Menentukan Nilai Akhir  ==========================================="<<endl;
	cout<<menentukan_nilai_akhir()<<endl;
	cout<<"=========================================2.Menentukan Tahun Kabisat  ========================================="<<endl;
	cout<<menentukan_tahun_kabisat();
}
