#include <iostream>
#include <conio.h>
#include <iomanip>

using namespace std;

int main(){
	//deklarasi //inisiasi
	int nilai[10] = {86,87,70,96,77};
	int jumlah=0;
	int max,min;
	float rata;
	//pengisian nilai
	nilai[5] = 90;
	//menampilkan nilai
	cout<<nilai[0]<<endl;
	cout<<nilai[2]<<endl;
	cout<<nilai[4]<<endl;
	//mengisi dengan cara diinput user
	cout<<"masukan nilai ke-8 : "; cin>>nilai[7];
	cout<<"masukan nilai ke-9 : "; cin>>nilai[8];
	cout<<"masukan nilai ke-10 : "; cin>>nilai[9];
	//mengisi dengan cara diinput user melalui pengulangan
	for(int i=0; i<10; i++){
		cout<<"masukan nilai ke-"<<i+1<<" : ";
		cin>>nilai[i];
	}
	//membuat rapih data yg tampil
	cout<<setw(4); cout<<"No.|";
	cout<<setw(10); cout<<"Nilai|"<<endl;
	cout<<"---|---------|"<<endl;
	//menampilkan data rapih menggunakan pengulangan
	min = nilai[0];
	for(int i=0; i<10; i++){
		cout<<setw(3); cout<<i+1<<"|";
		cout<<setw(9); cout<<nilai[i]<<"|"<<endl;
		jumlah += nilai[i];
		
		//nilai max
		if(nilai[i]>max)
			max = nilai[i];
		//nilai min
		if(nilai[i]<min)
			min = nilai[i];
		
	}
	rata=jumlah/10;
	cout<<"---|---------|"<<endl;
	cout<<setw(7); cout<<"Jumlah";
	cout<<setw(7); cout<<jumlah<<endl;
	
	cout<<setw(7); cout<<"rata";
	cout<<setw(7); cout<<rata<<endl;
	
	cout<<setw(7); cout<<"max";
	cout<<setw(7); cout<<max<<endl;
	
	cout<<setw(7); cout<<"min";
	cout<<setw(7); cout<<min<<endl;
}
