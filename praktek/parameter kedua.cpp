#include <iostream>
#include <conio.h>
#include <string>
#include <iomanip>

using namespace std;

//1. menampilkan nama dengan parameter
string nama(string namaku){
	return namaku;
}
//2.mengembalikan luas lingkaran
double luasling(double r){
	double phi = 22/7;
	return phi * r * r;
}
//3.menghitung diskon
string diskon(double jumlahbayar){
	if(jumlahbayar>=5000000)
		return "Diskon 10%";
	else
		return "Diskon 5%";
}
//1.menampilkan inputan 2 angka
double angka(double angka1, double angka2){
	cout<<angka1<<","<<angka2<<endl;
	
}
//menjalankan
int main(){
	double r = 7;
	string namaku = "iqbal";
	double angka1,angka2;
	double jumlahbayar = 700000;

	cout<<"Nama Saya Adalah : "<<nama(namaku)<<endl;
	
	cout<<"Masukan Angka ke-1 = "; cin>>angka1;
	cout<<"Masukan Angka ke-2 = "; cin>>angka2;
	cout<<"Angka ke 1 dan 2 adalah :";
	cout<<angka(angka1,angka2);
	
	cout<<"Luas Lingkaran : ";
	cout<<luasling(r)<<endl;
	
	cout<<"Diskonnya adalah : ";
	cout<<diskon(jumlahbayar);

}
