#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

#define GRAVITASI 9.8

int main(){
	int nilai_a;
	float nilai_b;
	string nama;
	char asli;
	bool hasil = true;
	
	nilai_a=10;
	nilai_b=15.5;
	nama="santai";
	asli = 'a';	
	hasil = 2<5;
	
	cout<<"contoh konstanta Gravitasi: "<<GRAVITASI<<endl;
	cout<<"contoh integer: "<<nilai_a<<endl; 
	cout<<"contoh real: "<<nilai_b<<endl;
	cout<<"contoh string: "<<nama<<endl;
	cout<<"contoh char: "<<asli<<endl;
	cout<<"contoh boolean: "<<hasil<<endl;
}
