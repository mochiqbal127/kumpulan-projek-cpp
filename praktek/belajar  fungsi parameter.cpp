#include <iostream>
#include <conio.h>
#include <string>
#include <iomanip>

using namespace std;

//menampilkan kata
void kata(){
	cout<<"Hello world";
}
//mengembalikan nilai kata
string kata2(){
	return "Hello World";
}
//mengembalikan luas persegi
double luas(){
	double sisi = 10;
	
	return sisi * sisi;
}
//menggunakan parameter pada luas persegi
double luaspersegi(double sisi){
	return sisi*sisi;
}
//parameter inputan untuk nilai
double nilai_akhir(double uts, double uas, double harian){
	return (uts+uas+harian)/3;
}
//1. menampilkan nama dengan parameter
string nama(string namaku){
	return namaku;
}
//2.mengembalikan luas lingkaran
double luasling(double r){
	double phi = 22/7;
	return phi * r * r;
}
//3.menghitung diskon
//4.menampilkan inputan 2 angka
double angka(double angka1, double angka2){
	cout<<angka1<<","<<angka2<<endl;
	
}
//menjalankan
int main(){
	double r = 7;
	string namaku = "iqbal";
	double angka1,angka2;
	
	cout<<nama(namaku)<<endl;
	
	cout<<luasling(r)<<endl;
	

	cout<<"Masukan Angka ke-1 = "; cin>>angka1;
	cout<<"Masukan Angka ke-2 = "; cin>>angka2;
	cout<<angka(angka1,angka2);	
	
}
