#include <iostream>
#include <conio.h>
#include <string>
#include <iomanip>

using namespace std;

int main(){
	int umur[10];
	string nama[10];
	int jumlah=0;
	int min,max = 0;
	float rata;
	
	for(int b=0;b<10;b++){
		cout<<"Nama Ke-"<<b+1<<" : ";
		cin>>nama[b];
	}
	system("cls");
	for(int a=0; a<10;a++){
		cout<<"Masukan Umur ke-"<<a+1<<" : ";
		cin>>umur[a];
		
		jumlah += umur[a];
		
		rata=jumlah/10;
			
		if(umur[a] > max)
			max=umur[a];
		
		if(umur[a]<min)
			min=umur[a];
			
	}
	system("cls");
	cout<<setw(4); cout<<"No.|";
	cout<<setw(14); cout<<"Nama|";
	cout<<setw(17); cout<<"Umur|"<<endl;
	cout<<"---|-------------|----------------|"<<endl;
	for(int b=0 ; b<10 ; b++){
		cout<<setw(3); cout<<b+1<<"|";
		cout<<setw(13); cout<<nama[b]<<"|";
		cout<<setw(16); cout<<umur[b]<<"|"<<endl;
	}	
	cout<<"-----------------------------------"<<endl;

			
	cout<<setw(7); cout<<"Jumlah";
	cout<<setw(13); cout<<jumlah<<endl;
	
	cout<<setw(7); cout<<"rata";
	cout<<setw(13); cout<<rata<<endl;
	
	cout<<setw(7); cout<<"max";
	cout<<setw(13); cout<<max<<endl;
	
	cout<<setw(7); cout<<"min";
	cout<<setw(13); cout<<min<<endl;
}
