#include <conio.h>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

typedef struct{
	string kode;
	string jenis;
	int ukuran;
	double harga;
} susu;
susu data_susu[9];
int jml_susu = 9;

typedef struct{
	int no_penjualan;
	string jenis;
	string nama;
	int ukuran;
	double harga;
	int qty;
	double total;
	double disc;
}penjualan;
penjualan data_penjualan[100];
int jml_data;
penjualan temp;
double total_sub,total_potong,total_semua;

void menu(){
	system("CLS");
	cout<<"================================================================\n";
	cout<<"				MENU\n";
	cout<<"================================================================\n";
	cout<<"		1. Input Penjualan\n";
	cout<<"		2. Tampil Penjualan\n";
	cout<<"		3. Cari Penjualan\n";
	cout<<"		4. Urutkan Data(Total)\n";
	cout<<"		5. Export\n";
	cout<<"		6. Import\n";
	cout<<endl;
}
void isi(){
	data_susu[0].kode = "a";
	data_susu[0].jenis = "Dancow";
	data_susu[0].ukuran = 1;
	data_susu[0].harga = 15000;
	data_susu[1].kode = "b";
	data_susu[1].jenis = "Bendera";
	data_susu[1].ukuran = 1;
	data_susu[1].harga = 14000;
	data_susu[2].kode = "c";
	data_susu[2].jenis = "SGM";
	data_susu[2].ukuran = 1;
	data_susu[2].harga = 13000;
	data_susu[3].kode = "a";
	data_susu[3].jenis = "Dancow";
	data_susu[3].ukuran = 2;
	data_susu[3].harga = 20000;
	data_susu[4].kode = "b";
	data_susu[4].jenis = "Bendera";
	data_susu[4].ukuran = 2;
	data_susu[4].harga = 19000;
	data_susu[5].kode = "c";
	data_susu[5].jenis = "SGM";
	data_susu[5].ukuran = 2;
	data_susu[5].harga = 18000;
	data_susu[6].kode = "a";
	data_susu[6].jenis = "Dancow";
	data_susu[6].ukuran = 3;
	data_susu[6].harga = 25000;
	data_susu[7].kode = "b";
	data_susu[7].jenis = "Bendera";
	data_susu[7].ukuran = 3;
	data_susu[7].harga = 23000;
	data_susu[8].kode = "c";
	data_susu[8].jenis = "SGM";
	data_susu[8].ukuran = 3;
	data_susu[8].harga = 22000;
}
void menu_jual(){
	cout<<"Toko Kelontong Keroncongan\n";
	cout<<"----------------------------\n";
	cout<<"A. Susu Dancow\n	1. Ukuran Kecil		: 15000\n	2. Ukuran Sedang	: 20000\n	3. Ukuran Besar		: 25000\n";
	cout<<"B. Susu Bendera\n	1. Ukuran Kecil		: 14000\n	2. Ukuran Sedang	: 19000\n	3. Ukuran Besar		: 23000\n";
	cout<<"C. Susu SGM\n	1. Ukuran Kecil		: 13000\n	2. Ukuran Sedang	: 18000\n	3. Ukuran Besar		: 22000\n";
}
int search(susu a[], int jml, string kd,int ukur){
	for(int n = jml - 1 ; n> -2	; n--)
		if(n == -1 || ((a[n].kode == kd) && (a[n].ukuran == ukur)) )return n;
}
void input_jual(){
	string kode;
	int ukuran;
	char ulangi = 'y';
	int index = 0;
	string namap;
	int nopen;
	int beli;
	int idx = 0;
	double diskon;
	if(jml_data!=0){
		idx = jml_data - 1;
	}else{
		idx = 0;
	}
	do{
		system("CLS");
		menu_jual();
		cout<<"\nMasukan No Penjualan		: "; cin>>nopen; 
		cout<<"\nMasukan Nama Customer		: "; cin>>namap;
		cout<<"\nMasukan Jenis Susu		: "; cin>>kode;
		cout<<"\nMasukan Ukuran Kaleng		: "; cin>>ukuran;
		index = search(data_susu,jml_susu,kode,ukuran);
		cout<<"\nJenis Susu			: "<<data_susu[index].jenis; 
		if(ukuran == 1){
			cout<<"\nUkuran Susu			: Kecil"; 
		}
		else if(ukuran ==2){
			cout<<"\nUkuran Susu			: Sedang"; 
		}
		else{
			cout<<"\nUkuran Susu			: Besar"; 
		}
		cout<<"\nHarga Satuan Barang		: Rp. "<<data_susu[index].harga; 
		cout<<"\nMasukan Jumlah Yang dibeli	: "; cin>>beli;
		cout<<"\nSub Total			: Rp. "<<data_susu[index].harga*beli; 
		if(beli>=10 &&  ukuran == 3 ){
			cout<<"\nDiscount %			: 10%";
			diskon = (data_susu[index].harga * beli * 10)/100;
			cout<<"\nTotal Discount			: Rp. "<<diskon;
		}else if(beli >= 5 && ((ukuran == 3) || (ukuran == 2))){
			cout<<"\nDiscount %			: 5%";
			diskon = (data_susu[index].harga * beli * 0.05);
			cout<<"\nTotal Discount			: Rp. "<<diskon;
		}else if(beli >= 10 && ukuran == 1){
			cout<<"\nDiscount %			: 2%";
			diskon = (data_susu[index].harga * beli * 0.02);
			cout<<"\nTotal Discount			: Rp. "<<diskon;
		}
		else{
			cout<<"\nDiscount %		: Tidak ada Diskon";
			diskon = 0;
			cout<<"\nTotal Discount			: Tidak ada Potongan";
		}
		cout<<"\nTotal Bayar			: Rp. "<<(data_susu[index].harga*beli)-diskon;
		
		data_penjualan[idx].no_penjualan = nopen;
		data_penjualan[idx].nama = namap;
		data_penjualan[idx].jenis = data_susu[index].jenis;
		data_penjualan[idx].ukuran = data_susu[index].ukuran;
		data_penjualan[idx].harga = data_susu[index].harga;
		data_penjualan[idx].qty = beli;
		data_penjualan[idx].total = (data_susu[index].harga * beli) - diskon;
		if(beli>=10 && ukuran == 3 ){
			data_penjualan[idx].disc = 0.1;
		}else if(beli >= 5 && ((ukuran == 3) || (ukuran == 2))){
			data_penjualan[idx].disc = 0.05;
		}else if(beli >= 10 && ukuran == 1){
			data_penjualan[idx].disc = 0.02;
		}
		else{
			data_penjualan[idx].disc = 0;
		}
		idx++;
		cout<<"\nTambah Pembelian (y/n)	: "; cin>>ulangi;
	}
	while(ulangi=='y'||ulangi=='Y');
	if(jml_data!=0){
		jml_data = idx + 1;	
	}else{
		jml_data+=idx+1;
	}
//	jml_data=3;	
	
}


void tampil_jual(){
	total_sub = 0;
	total_potong =	0;
	total_semua =0;
	system("CLS");
	cout<<"Penjualan Susu Kotak\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"No_Penjualan\t Jenis\t Ukuran\t Harga\t QTY\t Subtotal\t Disc%\t Potongan\t Total\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	for(int a = 0 ; a<jml_data -1; a++){
		cout<<data_penjualan[a].no_penjualan<<"\t\t";
		cout<<data_penjualan[a].jenis<<"\t";
		if(data_penjualan[a].ukuran == 3){
			cout<<"Besar\t"; 
		}
		else if(data_penjualan[a].ukuran ==2){
			cout<<"Sedang\t"; 
		}
		else{
			cout<<"Kecil\t"; 
		}

		cout<<data_penjualan[a].harga<<"\t";
		cout<<data_penjualan[a].qty<<"\t";
		cout<<data_penjualan[a].harga*data_penjualan[a].qty<<"\t\t";
		if(data_penjualan[a].disc==0.1){
			cout<<"10%\t";
			
		}else if(data_penjualan[a].disc==0.05){
			cout<<"5%\t";
		}else if(data_penjualan[a].disc==0.02){
			cout<<"2%\t";
		}else{
			cout<<"0%\t";
		}
		cout<<data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc<<"\t\t";
		cout<<data_penjualan[a].total<<"\t\n";
		
		total_sub += data_penjualan[a].harga*data_penjualan[a].qty;
		total_potong +=	data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc;
		total_semua +=data_penjualan[a].total;
	}
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"Total		\t \t \t\tRp. "<<total_sub<<"\t \tRp. "<<total_potong<<"\tRp. "<<total_semua<<"\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
}
int cari(penjualan a[], int jml, int target){
	for(int n = jml - 1 ; n> -2	; n--)
		if(n == -1 || a[n].no_penjualan == target) return n;
}
void cari_jual(){
	system("CLS");
	char ulangi='y';
	int idx;
	int nopen;
	do{
		cout<<"\nNomor Penjualan	: "; cin>>nopen;
		idx = cari(data_penjualan,jml_data,nopen);
		cout<<"\nNama Customer	: "<<data_penjualan[idx].nama;
		cout<<"\nJenis		: "<<data_penjualan[idx].jenis;
		if(data_penjualan[idx].ukuran == 1){
				cout<<"\nUkuran		: Besar\t"; 
		}
		else if(data_penjualan[idx].ukuran ==2){
			cout<<"\nUkuran		: Sedang\t"; 
		}
		else{
			cout<<"\nUkuran		: Kecil\t"; 
		}
		cout<<"\nHarga		: Rp. "<<data_penjualan[idx].harga; 
		cout<<"\nQTY		: "<<data_penjualan[idx].qty;
		cout<<"\nSubtotal	: Rp. "<<data_penjualan[idx].harga*data_penjualan[idx].qty;
		if(data_penjualan[idx].disc==0.1){
			cout<<"\nDiskon		: 10%\t";
			
		}else if(data_penjualan[idx].disc==0.05){
			cout<<"\nDiskon		: 5%\t";
		}else if(data_penjualan[idx].disc==0.02){
			cout<<"\nDiskon		: 2%\t";
		}else{
			cout<<"\nDiskon		: 0%\t";
		} 
		cout<<"\nPotongan	: Rp. "<<data_penjualan[idx].harga*data_penjualan[idx].qty*data_penjualan[idx].disc;
		cout<<"\nTotal		: Rp. "<<data_penjualan[idx].total; 
		cout<<"\nCari Data Kembali (y/n) ? "; cin>>ulangi;
	}while(ulangi=='y'||ulangi=='Y');
	
}
int sorting(){
	for (int i = 1;i<jml_data;i++){
		for (int j=jml_data-1;j>=i;j--){
			if(data_penjualan[j].disc > data_penjualan[j-1].disc){
				temp = data_penjualan[j];
				data_penjualan[j] = data_penjualan[j-1];
				data_penjualan[j-1]= temp;
				
			}
		}
	}
}

void sort(){
	sorting();
	total_sub = 0;
	total_potong =	0;
	total_semua =0;
	system("CLS");
	cout<<"Penjualan Susu Kotak\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"No_Penjualan\t Jenis\t Ukuran\t Harga\t QTY\t Subtotal\t Disc%\t Potongan\t Total\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	for(int a = 0 ; a<jml_data -1; a++){
		cout<<data_penjualan[a].no_penjualan<<"\t\t";
		cout<<data_penjualan[a].jenis<<"\t";
		if(data_penjualan[a].ukuran == 3){
			cout<<"Besar\t"; 
		}
		else if(data_penjualan[a].ukuran ==2){
			cout<<"Sedang\t"; 
		}
		else{
			cout<<"Kecil\t"; 
		}

		cout<<data_penjualan[a].harga<<"\t";
		cout<<data_penjualan[a].qty<<"\t";
		cout<<data_penjualan[a].harga*data_penjualan[a].qty<<"\t\t";
		if(data_penjualan[a].disc==0.1){
			cout<<"10%\t";
			
		}else if(data_penjualan[a].disc==0.05){
			cout<<"5%\t";
		}else if(data_penjualan[a].disc==0.02){
			cout<<"2%\t";
		}else{
			cout<<"0%\t";
		}
		cout<<data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc<<"\t\t";
		cout<<data_penjualan[a].total<<"\t\n";
		
		total_sub += data_penjualan[a].harga*data_penjualan[a].qty;
		total_potong +=	data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc;
		total_semua +=data_penjualan[a].total;
	}
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	cout<<"Total		\t \t \t\tRp. "<<total_sub<<"\t \t Rp. "<<total_potong<<"\t Rp. "<<total_semua<<"\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	
}
void save(){
	system("CLS");
	ofstream MyFile("ExportUjikom.txt");
	if(jml_data != 0 && MyFile.is_open()){
		cout<<"Menyimpan data ke ExportUjikom.txt\n";
		cout<<"-----------------------------------------------------------------------------------------------------\n";
		cout<<"No_Penjualan\t Jenis\t Ukuran\t Harga\t QTY\t Subtotal\t Disc%\t Potongan\t Total\n";
		cout<<"-----------------------------------------------------------------------------------------------------\n";

		MyFile<<"-----------------------------------------------------------------------------------------------------\n";
		MyFile<<"No_Penjualan\t Jenis\t Ukuran\t Harga\t QTY\t Subtotal\t Disc%\t Potongan\t Total\n";
		MyFile<<"-----------------------------------------------------------------------------------------------------\n";
		for(int a = 0 ; a<jml_data-1 ; a++){
			cout<<data_penjualan[a].no_penjualan<<"\t\t";
			cout<<data_penjualan[a].jenis<<"\t";
			if(data_penjualan[a].ukuran == 3){
				cout<<"Besar\t"; 
			}
			else if(data_penjualan[a].ukuran ==2){
				cout<<"Sedang\t"; 
			}
			else{
				cout<<"Kecil\t"; 
			}
	
			cout<<data_penjualan[a].harga<<"\t";
			cout<<data_penjualan[a].qty<<"\t";
			cout<<data_penjualan[a].harga*data_penjualan[a].qty<<"\t\t";
			if(data_penjualan[a].disc==0.1){
				cout<<"10%\t";
				
			}else if(data_penjualan[a].disc==0.05){
				cout<<"5%\t";
			}else if(data_penjualan[a].disc==0.02){
				cout<<"2%\t";
			}else{
				cout<<"0%\t";
			}
			cout<<data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc<<"\t\t";
			cout<<data_penjualan[a].total<<"\t\n";
			
			MyFile<<data_penjualan[a].no_penjualan<<"\t\t";
			MyFile<<data_penjualan[a].jenis<<"\t";
			if(data_penjualan[a].ukuran == 3){
				MyFile<<"Besar\t"; 
			}
			else if(data_penjualan[a].ukuran ==2){
				MyFile<<"Sedang\t"; 
			}
			else{
				MyFile<<"Kecil\t"; 
			}
	
			MyFile<<data_penjualan[a].harga<<"\t";
			MyFile<<data_penjualan[a].qty<<"\t";
			MyFile<<data_penjualan[a].harga*data_penjualan[a].qty<<"\t\t";
			if(data_penjualan[a].disc==0.1){
				MyFile<<"10%\t";
				
			}else if(data_penjualan[a].disc==0.05){
				MyFile<<"5%\t";
			}else if(data_penjualan[a].disc==0.02){
				MyFile<<"2%\t";
			}else{
				MyFile<<"0%\t";
			}
			MyFile<<data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc<<"\t\t";
			MyFile<<data_penjualan[a].total<<"\t\n";
		}
		cout<<"-----------------------------------------------------------------------------------------------------\n";
		cout<<"Total		\t \t \t\tRp. "<<total_sub<<"\t \t Rp. "<<total_potong<<"\t Rp. "<<total_semua<<"\n";
		cout<<"-----------------------------------------------------------------------------------------------------\n";
		MyFile<<"-----------------------------------------------------------------------------------------------------\n";
		MyFile<<"Total		\t \t \t\tRp. "<<total_sub<<"\t \t Rp. "<<total_potong<<"\t Rp. "<<total_semua<<"\n";
		MyFile<<"-----------------------------------------------------------------------------------------------------\n";
	}
	
}
void read(){
	system("CLS");
	cout<<"Membaca isi ExportUjikom.txt\n\n";
	string myFile;
	ifstream MyReadFile("ExportUjikom.txt");
	while (getline (MyReadFile, myFile)) {
  		cout<<myFile<<endl;
	}
	MyReadFile.close();

}
int main(){
	isi();
	
	int pilihMenu;
	char backToMenu='y';
	system("COLOR F0");
	system("CLS");
	
	do{
		menu();
		cout<<endl<<"Pilih menu (1-7) : "; cin>>pilihMenu;
		
		switch(pilihMenu) {
			system("COLOR F0");
		case 1 :input_jual();
			break;			
		case 2 :tampil_jual();
			break;			
		case 3 :cari_jual();
			break;			
		case 4 :sort();
			break;			
		case 5 :save();
			break;		
		case 6 :read(); 
			break;			
		case 7 :return 0;
			break;
		default : cout<<"\nPilihan Menu Tidak Ada!\n"<<endl;
	}
		cout<<"\n\nKembali ke menu (y/n) : "; cin>>backToMenu;
	}while(backToMenu == 'y' || backToMenu == 'Y');
	
}
