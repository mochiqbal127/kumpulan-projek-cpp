#include <conio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

typedef struct{
	string kode;
	string jenis;
	int ukuran;
	double harga;
} barang;
barang data_barang[9];
int jml_barang = 9;

typedef struct{
	int no_penjualan;
	string jenis;
	string nama;
	int ukuran;
	double harga;
	int qty;
	double total;
	double disc;
}penjualan;
penjualan data_penjualan[100];
int jml_data;
penjualan temp;
double total_sub,total_potong,total_semua;

void menu(){
	system("CLS");
	cout<<"=====================================================================================\n";
	cout<<"			Menu Aplikasi Penjualan Ayam Potong\n";
	cout<<"=====================================================================================\n";
	cout<<"1. Input Data Penjualan\n";
	cout<<"2. Tampil Data Penjualan\n";
	cout<<"3. Cari Data Penjualan\n";
	cout<<"4. Urutkan Data Penjualan (Desc)\n";
	cout<<"5. Export Data Penjualan\n";
	cout<<"6. Import Data Penjualan\n";
	cout<<endl;
}
void isi(){
	data_barang[0].kode = "a";
	data_barang[0].jenis = "Ayam Boyler";
	data_barang[0].ukuran = 1;
	data_barang[0].harga = 50000;
	data_barang[1].kode = "b";
	data_barang[1].jenis = "Ayam Pejantan";
	data_barang[1].ukuran = 1;
	data_barang[1].harga = 70000;
	data_barang[2].kode = "c";
	data_barang[2].jenis = "Ayam KAMPUNG";
	data_barang[2].ukuran = 1;
	data_barang[2].harga = 90000;
	
	data_barang[3].kode = "a";
	data_barang[3].jenis = "Ayam Boyler";
	data_barang[3].ukuran = 2;
	data_barang[3].harga = 60000;
	data_barang[4].kode = "b";
	data_barang[4].jenis = "Ayam Pejantan";
	data_barang[4].ukuran = 2;
	data_barang[4].harga = 800000;
	data_barang[5].kode = "c";
	data_barang[5].jenis = "Ayam KAMPUNG";
	data_barang[5].ukuran = 2;
	data_barang[5].harga = 100000;
	
	data_barang[6].kode = "a";
	data_barang[6].jenis = "Ayam Boyler";
	data_barang[6].ukuran = 3;
	data_barang[6].harga = 70000;
	data_barang[7].kode = "b";
	data_barang[7].jenis = "Ayam Pejantan";
	data_barang[7].ukuran = 3;
	data_barang[7].harga = 90000;
	data_barang[8].kode = "c";
	data_barang[8].jenis = "Ayam KAMPUNG";
	data_barang[8].ukuran = 3;
	data_barang[8].harga = 110000;
	
//	data_penjualan[0].no_penjualan = 1;
//	data_penjualan[0].nama = "Iqbal";
//	data_penjualan[0].jenis = "Ayam Boyler";
//	data_penjualan[0].ukuran = 1;
//	data_penjualan[0].harga = 50000;
//	data_penjualan[0].qty = 10;
//	data_penjualan[0].disc = 0.05;
//	data_penjualan[0].total = 475000;
//	jml_data=2;
}
void menu_jual(){
	cout<<"Penjualan Ayam Potong\n";
	cout<<"----------------------------\n";
	cout<<"A. Ayam Boyler\n	1. Ukuran Kecil: 50.000\n	2. Ukuran Sedang: 60.000\n	3. Ukuran Besar: 70.000\n";
	cout<<"B. Ayam Pejantan\n	1. Ukuran Kecil: 70.000\n	2. Ukuran Sedang: 80.000\n	3. Ukuran Besar: 90.000\n";
	cout<<"C. Ayam Kampung\n	1. Ukuran Kecil: 90.000\n	2. Ukuran Sedang: 100.000\n	3. Ukuran Besar: 100.000\n";
}
int search(barang a[], int jml, string kd,int ukur){
	for(int n = jml - 1 ; n> -2	; n--)
		if(n == -1 || ((a[n].kode == kd) && (a[n].ukuran == ukur)) )return n;
}
void input_jual(){
	string kode;
	int ukuran;
	char ulangi = 'y';
	int index = 0;
	int nopen;
	string namap;
	int beli;
	int idx = 0;
	double diskon;
	if(jml_data!=0){
		idx = jml_data - 1;
	}else{
		idx = 0;
	}
	do{
		system("CLS");
		menu_jual();
		cout<<"\nMasukan Nomor Penjualan	: "; cin>>nopen; 
		cout<<"\nMasukan Nama Customer	: "; cin>>namap;
		cout<<"\nMasukan Jenis Ayam	: "; cin>>kode;		
		cout<<"\nMasukan Ukuran Ayam	: "; cin>>ukuran;
		index = search(data_barang,jml_barang,kode,ukuran);
		cout<<"\nJenis Ayam 		: "<<data_barang[index].jenis; 
		if(ukuran == 3){
			cout<<"\nUkuran Ayam		: Besar"; 
		}
		else if(ukuran ==2){
			cout<<"\nUkuran Ayam		: Sedang"; 
		}
		else{
			cout<<"\nUkuran Ayam		: Kecil"; 
		}
		cout<<"\nHarga barang		: "<<data_barang[index].harga; 
		cout<<"\nJumlah Beli		: "; cin>>beli;
		cout<<"\nJumlah Harga Awal	: "<<data_barang[index].harga*beli; 
		if(beli>=20){
			cout<<"\nDiskon			: 9%";
			diskon = (data_barang[index].harga * beli * 9)/100;
			cout<<"\nTotal Diskon		:"<<diskon;
		}else if(beli >= 15){
			cout<<"\nDiskon			: 7%";
			diskon = (data_barang[index].harga * beli * 0.07);
			cout<<"\nTotal Diskon		:"<<diskon;
		}else if(beli >= 10){
			cout<<"\nDiskon			: 5%";
			diskon = (data_barang[index].harga * beli * 0.05);
			cout<<"\nTotal Diskon		: "<<diskon;
		}
		else{
			cout<<"\nDiskon			: Tidak ada Diskon";
			diskon = 0;
			cout<<"\nPotongan		: Tidak ada Potongan";
		}
		cout<<"\nHarga Total		: "<<(data_barang[index].harga*beli)-diskon;
		
		data_penjualan[idx].no_penjualan = nopen;
		data_penjualan[idx].nama = namap;
		data_penjualan[idx].jenis = data_barang[index].jenis;
		data_penjualan[idx].ukuran = data_barang[index].ukuran;
		data_penjualan[idx].harga = data_barang[index].harga;
		data_penjualan[idx].qty = beli;
		data_penjualan[idx].total = (data_barang[index].harga * beli) - diskon;
		if(beli>=20 ){
			data_penjualan[idx].disc = 0.09;
		}else if(beli >= 15 ){
			data_penjualan[idx].disc = 0.07;
		}else if(beli >= 10 ){
			data_penjualan[idx].disc = 0.05;
		}
		else{
			data_penjualan[idx].disc = 0;
		}
		idx++;
		cout<<"\nTambah Pembelian (y/n)	: "; cin>>ulangi;
	}
	while(ulangi=='y'||ulangi=='Y');
	if(jml_data!=0){
		jml_data = idx + 1;	
	}else{
		jml_data+=idx+1;
	}
//	jml_data=3;	
	
}


void tampil_jual(){
	total_sub = 0;
	total_potong =	0;
	total_semua =0;
	system("CLS");
	cout<<"Data Pembelian\n";
	cout<<"------------------------------------\n\n";
	cout<<"No_Penjualan	Jenis\t Ukuran\t Harga\t Jumlah\t Subtotal\t Disc(%)\t Total_Disc\t Total_Bayar\n";
//	cout<<"No_Penjualan"<<setw(7)<<"Jenis"<<setw(13)<<"Ukuran"<<setw(7)<<"Harga"<<setw(7)<<" Jumlah Subtotal Disc(%) Total_Disc Total_Bayar\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	for(int a = 0 ; a<jml_data -1; a++){
		cout<<data_penjualan[a].no_penjualan<<"\t";
		cout<<data_penjualan[a].jenis<<"\t";
		if(data_penjualan[a].ukuran == 1){
			cout<<"Kecil\t"; 
		}
		else if(data_penjualan[a].ukuran ==2){
			cout<<"Sedang\t"; 
		}
		else{
			cout<<"Besar\t"; 
		}

		cout<<"Rp. "<<data_penjualan[a].harga<<"\t";
		cout<<data_penjualan[a].qty<<"\t";
		cout<<"Rp. "<<data_penjualan[a].harga*data_penjualan[a].qty<<"\t";
		if(data_penjualan[a].disc==0.09){
			cout<<"9%\t";
			
		}else if(data_penjualan[a].disc==0.07){
			cout<<"7%\t";
		}else if(data_penjualan[a].disc==0.05){
			cout<<"5%\t";
		}else{
			cout<<"0%\t";
		}
		cout<<"Rp. "<<data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc<<"\t";
		cout<<"Rp. "<<data_penjualan[a].total<<"\t\n";
		
		total_sub += data_penjualan[a].harga*data_penjualan[a].qty;
		total_potong +=	data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc;
		total_semua +=data_penjualan[a].total;
	}
	cout<<"Total		\t \t \t \t Rp. "<<total_sub<<"\t \t Rp. "<<total_potong<<"\t Rp. "<<total_semua<<"\n";
}
int cari(penjualan a[], int jml, int target){
	for(int n = jml - 1 ; n> -2	; n--)
		if(n == -1 || a[n].no_penjualan == target) return n;
}
void cari_jual(){
	system("CLS");
	char ulangi='y';
	int idx;
	int nopen;
	do{
		cout<<"\nNomor Penjualan	: "; cin>>nopen;
		idx = cari(data_penjualan,jml_data,nopen);
		cout<<"\nNama Pembeli	: "<<data_penjualan[idx].nama;
		cout<<"\nJenis		: "<<data_penjualan[idx].jenis;
		if(data_penjualan[idx].ukuran == 1){
				cout<<"\nUkuran		: Kecil\t"; 
		}
		else if(data_penjualan[idx].ukuran == 2){
			cout<<"\nUkuran		: Sedang\t"; 
		}
		else{
			cout<<"\nUkuran		: Besar\t"; 
		}
		cout<<"\nHarga		: "<<"Rp. "<<data_penjualan[idx].harga; 
		cout<<"\nQTY		: "<<data_penjualan[idx].qty;
		cout<<"\nHarga Awal	: "<<"Rp. "<<data_penjualan[idx].harga*data_penjualan[idx].qty;
		if(data_penjualan[idx].disc==0.09){
			cout<<"\nDiskon		: 9%\t";
			
		}else if(data_penjualan[idx].disc==0.07){
			cout<<"\nDiskon		: 7%\t";
		}else if(data_penjualan[idx].disc==0.05){
			cout<<"\nDiskon		: 5%\t";
		}else{
			cout<<"\nDiskon		: 0%\t";
		} 
		cout<<"\nPotongan	: "<<"Rp. "<<data_penjualan[idx].harga*data_penjualan[idx].qty*data_penjualan[idx].disc;
		cout<<"\nTotal		: "<<"Rp. "<<data_penjualan[idx].total; 
		cout<<"\nCari Data Kembali (y/n) ? "; cin>>ulangi;
	}while(ulangi=='y'||ulangi=='Y');
	
}
int sorting(){
	for (int i = 1;i<jml_data;i++){
		for (int j=jml_data-1;j>=i;j--){
			if(data_penjualan[j].total > data_penjualan[j-1].total){
				temp = data_penjualan[j];
				data_penjualan[j] = data_penjualan[j-1];
				data_penjualan[j-1]= temp;
				
			}
		}
	}
}

void sort(){
	sorting();
	total_sub = 0;
	total_potong =	0;
	total_semua =0;
	cout<<"Data Pembelian\n";
	cout<<"------------------------------------\n\n";
	cout<<"No_Penjualan Jenis\t Ukuran\t Harga\t Jumlah\t Subtotal\t Disc(%)\t Total_Disc\t Total_Bayar\n";
	cout<<"-----------------------------------------------------------------------------------------------------\n";
	for(int a = 0 ; a<jml_data-1 ; a++){
		cout<<data_penjualan[a].no_penjualan<<"\t";
		cout<<data_penjualan[a].jenis<<"\t";
		if(data_penjualan[a].ukuran == 1){
			cout<<"Kecil\t"; 
		}
		else if(data_penjualan[a].ukuran ==2){
			cout<<"Sedang\t"; 
		}
		else{
			cout<<"Besar\t"; 
		}

		cout<<"Rp. "<<data_penjualan[a].harga<<"\t";
		cout<<data_penjualan[a].qty<<"\t";
		cout<<"Rp. "<<data_penjualan[a].harga*data_penjualan[a].qty<<"\t";
		if(data_penjualan[a].disc==0.09){
			cout<<"9%\t";
			
		}else if(data_penjualan[a].disc==0.07){
			cout<<"7%\t";
		}else if(data_penjualan[a].disc==0.05){
			cout<<"5%\t";
		}else{
			cout<<"0%\t";
		}
		cout<<"Rp. "<<data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc<<"\t";
		cout<<"Rp. "<<data_penjualan[a].total<<"\t\n";
		
		total_sub += data_penjualan[a].harga*data_penjualan[a].qty;
		total_potong +=	data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc;
		total_semua +=data_penjualan[a].total;
	}
	cout<<"Total		\t \t \t \t Rp. "<<total_sub<<"\t \t Rp. "<<total_potong<<"\t Rp. "<<total_semua<<"\n";
	
}
void save(){
	system("CLS");
	ofstream MyFile("FileExport.txt");
	total_sub= 0;
	total_potong =	0;
	total_semua =0;
	if(jml_data != 0 && MyFile.is_open()){
		cout<<"Menyimpan data ke FileExport.txt\n";
		cout<<"No_Penjualan Jenis\t Ukuran\t Harga\t Jumlah\t Subtotal\t Disc(%)\t Total_Disc\t Total_Bayar\n";
		MyFile<<"No_Penjualan Jenis\t Ukuran\t Harga\t Jumlah\t Subtotal\t Disc(%)\t Total_Disc\t Total_Bayar\n";
		for(int a = 0 ; a<jml_data-1 ; a++){
			cout<<data_penjualan[a].no_penjualan<<"\t";
			cout<<data_penjualan[a].jenis<<"\t";
			if(data_penjualan[a].ukuran == 1){
				cout<<"Kecil\t"; 
			}
			else if(data_penjualan[a].ukuran ==2){
				cout<<"Sedang\t"; 
			}
			else{
				cout<<"Besar\t"; 
			}
	
			cout<<"Rp. "<<data_penjualan[a].harga<<"\t";
			cout<<data_penjualan[a].qty<<"\t";
			cout<<"Rp. "<<data_penjualan[a].harga*data_penjualan[a].qty<<"\t";
			if(data_penjualan[a].disc==0.09){
				cout<<"9%\t";
				
			}else if(data_penjualan[a].disc==0.07){
				cout<<"7%\t";
			}else if(data_penjualan[a].disc==0.05){
				cout<<"5%\t";
			}else{
				cout<<"0%\t";
			}
			cout<<"Rp. "<<data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc<<"\t";
			cout<<"Rp. "<<data_penjualan[a].total<<"\t\n";
			
			total_sub += data_penjualan[a].harga*data_penjualan[a].qty;
			total_potong +=	data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc;
			total_semua +=data_penjualan[a].total;
			
			MyFile<<data_penjualan[a].no_penjualan<<"\t";
			MyFile<<data_penjualan[a].jenis<<"\t";
			if(data_penjualan[a].ukuran == 1){
				MyFile<<"Kecil\t"; 
			}
			else if(data_penjualan[a].ukuran ==2){
				MyFile<<"Sedang\t"; 
			}
			else{
				MyFile<<"Besar\t"; 
			}
	
			MyFile<<"Rp. "<<data_penjualan[a].harga<<"\t";
			MyFile<<data_penjualan[a].qty<<"\t";
			MyFile<<"Rp. "<<data_penjualan[a].harga*data_penjualan[a].qty<<"\t";
			if(data_penjualan[a].disc==0.09){
				MyFile<<"9%\t";
				
			}else if(data_penjualan[a].disc==0.07){
				MyFile<<"7%\t";
			}else if(data_penjualan[a].disc==0.05){
				MyFile<<"5%\t";
			}else{
				MyFile<<"0%\t";
			}
			MyFile<<"Rp. "<<data_penjualan[a].harga*data_penjualan[a].qty*data_penjualan[a].disc<<"\t";
			MyFile<<"Rp. "<<data_penjualan[a].total<<"\t\n";
		}
		cout<<"Total		\t \t \t \t Rp. "<<total_sub<<"\t \t Rp. "<<total_potong<<"\t Rp. "<<total_semua<<"\n";
		MyFile<<"Total		\t \t \t \t Rp. "<<total_sub<<"\t \t Rp. "<<total_potong<<"\t Rp. "<<total_semua<<"\n";
	}
	
}
void read(){
	system("CLS");
	cout<<"Membaca isi FileExport.txt\n\n";
	string myFile;
	ifstream MyReadFile("FileExport.txt");
	while (getline (MyReadFile, myFile)) {
  		cout<<myFile<<endl;
	}
	MyReadFile.close();

}
int main(){
	isi();
	
	int pilihMenu;
	char backToMenu='y';
	system("COLOR F0");
	system("CLS");
	
	do{
		menu();
		cout<<endl<<"Pilih menu (1-6) : "; cin>>pilihMenu;
		
		switch(pilihMenu) {
			system("COLOR F0");
		case 1 :input_jual();
			break;			
		case 2 :tampil_jual();
			break;			
		case 3 :cari_jual();
			break;			
		case 4 :sort();
			break;			
		case 5 :save();
			break;		
		case 6 :read(); 
			break;			
		case 7 :return 0;
			break;
		default : cout<<"\nPilihan Menu Tidak Ada!\n"<<endl;
	}
		cout<<"\n\nKembali ke menu (y/n) : "; cin>>backToMenu;
	}while(backToMenu == 'y' || backToMenu == 'Y');
	
}
