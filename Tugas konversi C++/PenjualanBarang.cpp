#include <iostream>
#include <conio.h>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

typedef struct{
	string kode;
	string nama_barang;
	double harga;
	int qty;
} barang;
barang data_barang[100];
int jml_data_barang;

typedef struct{
	int idpel;
	string nama_pelanggan;
	string alamat;
	string no_hp;
} pelanggan;
pelanggan data_pelanggan[100];
int jml_data_pelanggan;

typedef struct{
	int id_penjualan;
	int idpel;
	string nama_pelanggan;
	int kode;
	string nama_barang;
	int jumlah;
	double total;
}penjualan;
penjualan data_penjualan[100];
int jml_data_penjualan;

void menu(){
	system("CLS");
	cout<<"============================\n";
	cout<<"MENU\n";
	cout<<"============================\n";
	cout<<"1. Entry Data Barang\n";
	cout<<"2. Tampil Data Barang\n";
	cout<<"3. Edit Data Barang\n";
	cout<<"4. Entry Data Pelanggan\n";
	cout<<"5. Tampil Data Pelanggan\n";
	cout<<"6. Edit Data Pelanggan\n";
	cout<<"7. Transaksi Penjualan\n";
	cout<<"8. Histori Penjualan\n";
	cout<<"9. Simpan data ke file\n";
	cout<<"10. Baca data dari file\n";
	cout<<"0. Keluar\n";
	cout<<endl;
}
int search(barang a[], int jml, string target){
	for(int n = jml - 1 ; n> -2	; n--)
		if(n == -1 || a[n].kode == target) return n;
}
void input_barang(){
	system("CLS");
	char ulangi='y';
	int a = 0;
	do{
		cout<<endl;
		cout<<"Kode	: "; cin>>data_barang[a].kode;
		cout<<"Nama Barang	: "; cin>>data_barang[a].nama_barang;
		cout<<"QTY	: "; cin>>data_barang[a].qty;
		cout<<"Harga	: "; cin>>data_barang[a].harga;
		a++;
		cout<<"\nTambah data (y/n)	: "; cin>>ulangi;
	}while(ulangi=='y'||ulangi=='Y');
	jml_data_barang += a;
}
void tampil_barang(){
	system("CLS");
	cout<<"Data Semua Barang\n";
	cout<<"------------------------------------\n\n";
	cout<<"No. Kode\t Nama Barang\t QTY\t Harga\n";
	cout<<"-------------------------------------------------------------------------------------------\n";
	for(int a = 0 ; a<jml_data_barang ; a++){
		cout<<a+1<<" ";
		cout<<data_barang[a].kode<<"\t";
		cout<<data_barang[a].nama_barang<<"\t";
		cout<<data_barang[a].qty<<"\t";
		cout<<data_barang[a].harga<<"\t\n";
	}
}
void edit_barang(){
	system("CLS");
	char ulangi = 'y';
	string kode;
	int index = 0;
	tampil_barang();
	do{
		cout<<"\n\nKode	: "; cin>>kode;
		index = search(data_barang,jml_data_barang,kode);
		cout<<"Nama	("<<data_barang[index].nama_barang<<") : "; cin>>data_barang[index].nama_barang;
		cout<<"\nTambah Data (y/n)	: "; cin>>ulangi;
	}while(ulangi=='y'||ulangi=='Y');
}
void input_pelanggan(){
	system("CLS");
	char ulangi='y';
	int a = 0;
	do{
		cout<<endl;
		cout<<"Id	: "; cin>>data_pelanggan[a].idpel;
		cout<<"Nama	: "; cin>>data_pelanggan[a].nama_pelanggan;
		cout<<"Alamat	: "; cin>>data_pelanggan[a].alamat;
		cout<<"No. HP	: "; cin>>data_pelanggan[a].no_hp;
		a++;
		cout<<"\nTambah data (y/n)	: "; cin>>ulangi;
	}while(ulangi=='y'||ulangi=='Y');
	jml_data_pelanggan += a;
}
void tampil_pelanggan(){
	system("CLS");
	cout<<"Data Pelanggan\n";
	cout<<"------------------------------------\n\n";
	cout<<"No. ID\t Nama\t Alamat\t No. HP\n";
	cout<<"-------------------------------------------------------------------------------------------\n";
	for(int a = 0 ; a<jml_data_pelanggan ; a++){
		cout<<a+1<<" ";
		cout<<data_pelanggan[a].idpel<<"\t";
		cout<<data_pelanggan[a].nama_pelanggan<<"\t";
		cout<<data_pelanggan[a].alamat<<"\t";
		cout<<data_pelanggan[a].no_hp<<"\t\n";
	}
}
void edit_pelanggan(){}
void entry_penjualan(){

}
void entry_pembayaran(){
	
}
void histori(){}
void savefile(){}
void readfile(){}
int main(){
	int pilihMenu;
	char backToMenu='y';
	system("COLOR F0");
	system("CLS");
	
	do{
		menu();
		cout<<endl<<"Pilih menu (1-7) : "; cin>>pilihMenu;
		
		switch(pilihMenu) {
			system("COLOR F0");
		case 1 :input_barang();
			break;			
		case 2 :tampil_barang();
			break;			
		case 3 :edit_barang();
			break;			
		case 4 :input_pelanggan();
			break;			
		case 5 :tampil_pelanggan();
			break;		
		case 6 :edit_pelanggan(); 
			break;			
		case 7 :entry_penjualan();
			break;
		case 8 :histori();
			break;
		case 9 :savefile();
			break;
		case 10 :readfile();
			break;
		case 11 :return 0;
			break;
		default : cout<<"\nPilihan Menu Tidak Ada!\n"<<endl;
	}
		cout<<"Kembali ke menu (y/n) : "; cin>>backToMenu;
	}while(backToMenu == 'y' || backToMenu == 'Y');
	
}
