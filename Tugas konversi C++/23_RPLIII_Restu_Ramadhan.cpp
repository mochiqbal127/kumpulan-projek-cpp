#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

void MencariLuasdankelilingLingkaran(){
		int r;
		float phi,luas,keliling;
			cout<<"--------------------------------------------- Mencari Luas dan Keliling Lingkaran ------------------------------------"<<endl;
			cout<<"                                                                                                                      "<<endl;
			cout<<"masukkan panjang jari-jari = ";
			cin>>r;
			phi=22/7;
			luas=phi*r*r;
			keliling=2*phi*r;
			cout<<"luas lingkaran     = "<<luas<<endl;
			cout<<"keliling lingkaran = "<<keliling<<endl;
	}
void MencariLuasdanKelilingPersegi(){
		float s,l,k;	
			cout<<"-------------------------------------------- Mencari Luas dan Keliling Persegi ---------------------------------------"<<endl;
			cout<<"                                                                                                                      "<<endl;
			cout<<"Masukkan Sisi Persegi = ";
			cin>>s;
			l = s*s;
			k = 4*s;
			cout<<"Luas Persegi dari panjang sisi adalah     = "<<l<<endl;
			cout<<"Keliling Persegi dari panjang sisi adalah = "<<k<<endl;
	}
void MencariLuasdaKelilingPersegiPanjang() {
		float p,l,luas,k;
		int r,i,z;
			cout<<"-------------------------------------- Mencari Luas dan Keliling Persegi Panjang ---------------------------------------"<<endl;
			cout<<"                                                                                                                        "<<endl;
			cout<<"Masukkan Panjang Persegi = ";
			cin>>p;
			cout<<"Masukkan Lebar Persegi   = ";
			cin>>l;
			luas = p*l;
			k = 2*(p+l);
			cout<<"Luas Persegi Panjang Adalah     = "<<luas<<endl;
			cout<<"Keliling Persegi Panjang Adalah = "<<k<<endl;
	}
void MencariLuasSegitiga(){
		float alas,tinggi,luas;
			cout<<"------------------------------------------------- Mencari Luas Segitiga ------------------------------------------------"<<endl;
			cout<<"                                                                                                                        "<<endl;
			cout<<"Masukkan Alas   = ";
			cin>>alas;
			cout<<"Masukkan Tinggi = ";
			cin>>tinggi;
			luas = 0.5 * alas * tinggi;
			cout<<"Luasnya adalah  = "<<luas<<endl;	
	}
void MencariVolumeKubus(){
		int sisi,volume;	
			cout<<"--------------------------------------------- Mencari Volume Kubus ----------------------------------------------------"<<endl;
			cout<<"                                                                                                                       "<<endl;
			cout<<"Masukkan sisi Kubus : ";
			cin>>sisi;
			volume = sisi*sisi*sisi;
			cout<<"Volume Kubus adalah : "<<volume<<endl;
	
	}
void MencariLuasJajarGenjang(){
			
		float alas,tinggi,luas;	
			cout<<"------------------------------------------- Mencari Luas JajarGenjang  -------------------------------------------------"<<endl;
			cout<<"                                                                                                                        "<<endl;
			cout<<"Masukan Alas   = ";
			cin>>alas;
			cout<<"Masukan Tinggi = ";
			cin>>tinggi;
			luas = alas*tinggi;
			cout<<"Luasnya adalah = "<<luas<<endl;
	
	
	}
void MenentukanFpb(){
		int v,s,t;
			cout<<"-----------------------------------------------Mencari Kelajuan Rata-rata ----------------------------------------------"<<endl;
			cout<<"                                                                                                                        "<<endl;
			cout<<"Masukan Jarak Tempuh (Meter) = ";
    		cin>>s;
    		cout<<"Masukan Waktu Tempuh (Detik) = ";
    		cin>>t;
    		v=s/t;
			cout<<"Kelajuan Rata-ratanya adalah = "<<v<<"m/s"<<endl;	
	}
void MencariNilaiRataRata(){
		int inggris,indonesia,mtk,ipa,jml,rata2;
			cout<<"----------------------------------------------- Mencari Nilai Rata - Rata ----------------------------------------------"<<endl;
			cout<<"                                                                                                                        "<<endl;
			cout<<"Nilai Bahasa Inggris = ";
			cin>>inggris;
			cout<<"Masukkan Nilai Bahasa Indonesia = ";
			cin>>indonesia;
			cout<<"Masukkan Nilai Matematika = ";
			cin>>mtk;
			cout<<"Masukkan Nilai IPA        = ";
			cin>>ipa;
			
			jml=inggris+indonesia+mtk+ipa;
			rata2 = jml/4;
			
			cout<<"Rata-Rata Nilai dari 4 Pelajaran adalah "<<rata2<<endl;
	}
void MembuatDeretFibonacchi(){
		int suku,bilangan1,bilangan2,c,i;	
			cout<<"------------------------------------------------- Membuat Deret Fibonacchi ---------------------------------------------"<<endl;
     		cout<<"                                                                                                                        "<<endl;
			cout<<"Masukkan nilai suku ke-: ";cin>>suku;
     		cout<<"Bilangannya adalah: "<<endl;
     		bilangan1=0;
     		bilangan2=1;
    		cout<<bilangan1<<endl;
			cout<<bilangan2<<endl;
     		for(int i=3; i<=suku; i++)
    		{
    			c = bilangan1 + bilangan2;
    			bilangan1 = bilangan2;
    			bilangan2 = c;

     			cout<<c<<endl;
     		}
	}
void MencariEnergiKinetik (){
		float massa,kecepatan,ep;	
			cout<<"----------------------------------------------------- Mencari Energi Kinetik -------------------------------------------"<<endl;
     		cout<<"                                                                                                                        "<<endl;
			cout<<"Masukkan berat massanya (dalam kg) = "<<endl;
     		cout<<"(Jika beratnya 250 gram ubah terlebih dahulu ke kilogram  maka diketik 0.25 (menggunakan titik bukan koma)"<<endl;
     		cout<<"Masukkan massannya = ";
     		cin>>massa;
     		cout<<"     "<<endl;
     		cout<<"masukkan Kecepatannya"<<endl;
     		cout<<"(jika 1,5 maka ditulisnya menggunakan (.) bukan (,) contoh 1,5 menjadi >>> 1.5)"<<endl;
     		cout<<"Masukkan Kecepatannya = ";
			cin>>kecepatan;
     		ep=0.5*massa*kecepatan*kecepatan;
     		cout<<"Energi Kinetik dari massa sebesar "<<massa<<" kg dengan "<<kecepatan<<" m/s adalah = "<<ep<<" joule"<<endl;
	}
void Exit(){
		int a;	
			cout<<"Apakah anda ingin keluar dari program "<<endl;
     		cout<<"{y} ya atau {n} tidak : ";
     		cin>>a;
     		if (a == 'y' || a == 'Y'){
     			exit(0);
			 }
			 else if ( a == 'n' || a == 'N'){
			 	cout<<"Kembali ke menu utama"<<endl;
			 }
			 else {
			 	cout<<"Pilihan yang anda masukkan tidak ada"<<endl;
     		}
	}
int main () {
	system ("Color F0");
	int pil;
	char BackToMenu,a;	
	BackToMenu = 'y';
	
	do{
	
	cout<<"========================================================================================================================"<<endl;
	cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
	cout<<"                                                                                                                        "<<endl;
	cout<<"                                  			MENU PROGRAM                                                               "<<endl;
	cout<<"                                                                                                                        "<<endl;
	cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<endl;
	cout<<"========================================================================================================================"<<endl;
	cout<<"                                                                                                                        "<<endl;
	cout<<"Pilihan 1 : Mencari Luas dan Keliling Lingkaran                                                                         "<<endl;
	cout<<"Pilihan 2 : Mencari Luas dan keliling Persegi                                                                           "<<endl;
	cout<<"Pilihan 3 : Mencari Luas dan Keliling Persegi Panjang                                                                   "<<endl;
	cout<<"Pilihan 4 : Mencari Luas Segitiga                                                                                       "<<endl;
	cout<<"Pilihan 5 : Mencari Volume Kubus                                                                                        "<<endl;
	cout<<"Pilihan 6 : Mencari Luas JajarGenjang                                                                                   "<<endl;
	cout<<"Pilihan 7 : Mencari Kelajuan Rata-Rata                                                                                  "<<endl;
	cout<<"Pilihan 8 : Mencari Nilai Rata-Rata untuk 4 Mata Pelajaran (Bahasa Indonesia, Inggris, Matematika, IPA)                 "<<endl;
	cout<<"Pilihan 9 : Membuat Deret Fibonacchi                                                                                    "<<endl;
	cout<<"Pilihan 10: Mencari Energi Kinetik (Ep)                                                                                 "<<endl;
	cout<<"Pilihan 11: Exit Program                                                                                                "<<endl;
	
	cout<<"                             			   Masukkan pilihan : ";
	cin>>pil;
	system("CLS");
	switch(pil) {
		case 1 :MencariLuasdankelilingLingkaran();
			break;
				
		case 2 :MencariLuasdanKelilingPersegi();
			break;
			
		case 3 :MencariLuasdaKelilingPersegiPanjang();
			break;
			
		case 4 : MencariLuasSegitiga();
			break;
			
		case 5 :MencariVolumeKubus();
			break;
		
		case 6 :MencariLuasJajarGenjang(); 
			break;
			
		case 7 :MenentukanFpb();
			break;
		
		case 8 :MencariNilaiRataRata();
			break;
			
		case 9 :MembuatDeretFibonacchi();
			break;
     		
     	case 10 :MencariEnergiKinetik ();
     		break;
     			
     	case 11 :Exit(); 
			break;
			 
		default : cout<<"Angka yang anda ketik tidak tersedia sebagaimana dicantumkan dalam program diatas"<<endl;
		
	}
	cout<<"Back To Menu (y/n)";
	cin>>BackToMenu;
}while(BackToMenu == 'y' || BackToMenu == 'Y');
getche();
}

